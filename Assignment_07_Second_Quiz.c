#include <stdio.h>
#include <stdlib.h>

int main()
{
    char s[100], chr;
    int c=0, j;

    printf("Enter a String:\n");
    gets(s);
    printf("Enter a Character to find it's Frequency:\n");
    scanf("%c",&chr);

    for(j= 0; s[j]!= '\0'; ++j)
        {
            if(chr==s[j])
            {
                ++c;
            }

        }
     printf("Frequency of the given character is %c= %d",chr,c);
     return 0;
}
