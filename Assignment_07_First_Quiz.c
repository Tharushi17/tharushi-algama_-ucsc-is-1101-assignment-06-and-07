#include <stdio.h>
#include <string.h>


int main()
{
    char str[100], reverse[100];
    int len, a, count=0, wbegin, wend;

    printf("Enter a Sentence:\n ");
    gets(str);

    len   = strlen(str);



    wbegin = len - 1;
    wend   = len - 1;

    while(wbegin > 0)
    {

        if(str[wbegin] == ' ')
        {

            a = wbegin + 1;
            while(a <= wend)
            {
                reverse[count] = str[a];

                a++;
                count++;
            }
            reverse[count++] = ' ';

            wend = wbegin - 1;
        }

        wbegin--;
    }


    for(a=0; a<=wbegin; a++)
    {
        reverse[count] = str[a];
        count++;
    }


    reverse[count] = '\0';

    printf("Original sentence is: \n%s\n\n", str);
    printf("Reverse ordered words of the sentence are: \n%s", reverse);

    return 0;
}
