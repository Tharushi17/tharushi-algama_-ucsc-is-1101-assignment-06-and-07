#include <stdio.h>

int main()
{
  int r1, c1, r2, c2, i, d, k, tot = 0;
  int fir[10][10], sec[10][10], mult[10][10];

  printf("Enter number of rows and columns of first matrix:\n");
  scanf("%d%d", &r1, &c1);
  printf("Enter elements of first matrix:\n");

  for (i = 0; i < r1; i++)
    for (d = 0; d < c1; d++)
      scanf("%d", &fir[i][d]);

  printf("\nEnter number of rows and columns of second matrix:\n");
  scanf("%d%d", &r2, &c2);

  if (c1 != r2)
    printf("The multiplication isn't possible.\n");
  else
  {
    printf("Enter elements of second matrix:\n");

    for (i = 0; i < r2; i++)
      for (d = 0; d < c2; d++)
        scanf("%d", &sec[i][d]);

    for (i = 0; i < r1; i++) {
      for (d = 0; d < c2; d++) {
        for (k = 0; k < r2; k++) {
          tot = tot + fir[i][k]*sec[k][d];
        }

        mult[i][d] = tot;
        tot = 0;
      }
    }

    printf("Product of the matrices:\n");

    for (i = 0; i < r1; i++) {
      for (d = 0; d < c2; d++)
        printf("%d\t", mult[i][d]);

      printf("\n");
    }
  }

  return 0;
}
