#include <stdio.h>
void fibonacciSeq(int n);
void fibonacciSeq(int n)
{
    int c,tot=0,num1=0,num2=1;
    for (c=0; c<n; c++)
    {
      if(c<=1)
      {
        tot=c;
      }
      else
      {
         tot=num1+num2;
         num1=num2;
         num2=tot;
      }
      printf("%d\n",tot);
    }

}

int main()
{
    int n;
    printf("Enter the number of terms: ");
    scanf("%d",&n);
    fibonacciSeq(n+1);

    return 0;

}


